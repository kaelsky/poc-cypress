import { Given } from "cypress-cucumber-preprocessor/steps";

Given(`I visit {string}`, string => {
  cy.visit(string);
});
