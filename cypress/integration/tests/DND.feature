Feature: Drag n Drop
  In order to drag things
  As a user
  I want to be able DnD

  @focus
  Scenario: Drag and drop image
    Given I visit "https://www.w3schools.com/html/html5_draganddrop.asp"
    And I see image in left box
    When I drag image from left box
    And I drop image to right box
    Then Image should be in right box