import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";

// prevents failing on uncaught exception
Cypress.on("uncaught:exception", () => false);

When(`I search for "Signing my first signature procedure"`, () => {
  cy.get("input#query").type("Signing my first signature procedure");
});

And("I press Enter", () => {
  cy.get("input#query").type("{enter}");
});

Then(`"Signing my first signature procedure" should be my first result`, () => {
  cy.get("ul li.search-result:first-child").contains(
    "Signing my first signature procedure"
  );
});
