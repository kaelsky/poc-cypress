import { When, And, Then } from "cypress-cucumber-preprocessor/steps";

const dataTransfer = new DataTransfer();

And("I see image in left box", () => {
  cy.get("#div1").find("img");
});

When(`I drag image from left box`, () => {
  cy.get("#div1 img").trigger("dragstart", { dataTransfer });
});

And("I drop image to right box", () => {
  cy.get("#div2").trigger("drop", { dataTransfer });
});

Then(`Image should be in right box`, () => {
  cy.get("#div2").find("#drag1");
});
